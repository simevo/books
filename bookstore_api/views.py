from django_filters import rest_framework as filters
from rest_framework import permissions, viewsets

from books import models
from bookstore_api.serializers import (
    AuthorSerializer,
    BookSerializer,
    PublisherSerializer,
)


class ReadOnly(permissions.BasePermission):
    def has_permission(self, request, view):
        return request.method in permissions.SAFE_METHODS


class PublisherView(viewsets.ModelViewSet):
    queryset = models.Publisher.objects.all()
    serializer_class = PublisherSerializer
    permission_classes = [ReadOnly]


class AuthorView(viewsets.ModelViewSet):
    queryset = models.Author.objects.all()
    serializer_class = AuthorSerializer
    permission_classes = [ReadOnly]


class BookFilter(filters.FilterSet):
    price_min = filters.NumberFilter(field_name="price", lookup_expr="gte")
    price_max = filters.NumberFilter(field_name="price", lookup_expr="lte")

    class Meta:
        model = models.Book
        fields = ["publisher"]


class BookView(viewsets.ModelViewSet):
    queryset = models.Book.objects.all()
    serializer_class = BookSerializer
    filterset_class = BookFilter
    permission_classes = [ReadOnly]
