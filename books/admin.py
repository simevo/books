from django.contrib import admin

from books.models import Book


class BookAdmin(admin.ModelAdmin):
    list_display = (
        "title",
        "authors",
        "price",
    )
    list_filter = ("authors",)
    readonly_fields = ("pk",)


admin.site.register(Book, BookAdmin)
